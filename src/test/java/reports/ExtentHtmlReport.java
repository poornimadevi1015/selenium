package reports;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentHtmlReport {
	static ExtentReports exReport;
	static ExtentHtmlReporter htmlReport;

	@BeforeSuite
	public void StartHtmlReport() {
		// template to create blank html report
		htmlReport = new ExtentHtmlReporter("./reports/basicreport.html");
		// Append the report in the existing html file it will keep the old logs
		htmlReport.setAppendExisting(true);
		// Object to edit /modify the report
		exReport = new ExtentReports();
		// Attach the exReport to the htmlReporter
		exReport.attachReporter(htmlReport);
	}

	@Test
	public void TestCaseReport() throws IOException {

		ExtentTest Test = exReport.createTest("TC001", "Create TestLead");
		Test.pass("Step Pass", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		Test.fail("Step Fails", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img10.png").build());
		Test.skip("Step Skipped");
		Test.assignAuthor("Narasimman");
		Test.assignCategory("Smoke");
		
		
		ExtentTest Test1 = exReport.createTest("TC002", "Create TestLead");
		Test1.pass("Step Pass", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		Test1.pass("Step Fails", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img10.png").build());
		Test1.pass("Step Skipped");
		Test1.assignAuthor("Narasimman");
		Test1.assignCategory("Smoke");

	}

	@AfterSuite
	public void EndHtmlReport() {
		exReport.flush();
	}

}
