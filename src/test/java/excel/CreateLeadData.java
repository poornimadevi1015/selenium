package excel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class CreateLeadData {

	public static void main(String[] args) throws IOException {

		// To open the xlsx sheet from the corresponding location

		XSSFWorkbook book = new XSSFWorkbook("./data/CreateLeadData.xlsx");

		// To go to the sheet name

		XSSFSheet sheet = book.getSheet("Data Create Lead");

		// To count the row

		int rowCount = sheet.getLastRowNum();

		System.out.println("Row Count is " + rowCount);

		// To count the column

		int columnCount = sheet.getRow(0).getLastCellNum();
		
		System.out.println("Column count is "+columnCount);

		// To fetch all the data from the excel

		for (int i = 0; i < rowCount; i++) {

			XSSFRow row = sheet.getRow(i);

			for (int j = 0; j < columnCount; j++) {

				XSSFCell cell = row.getCell(j);

				String stringCellValue = cell.getStringCellValue();

				System.out.println(stringCellValue);

			}
		}

	}

}
