package excel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnExcel {

	public static Object[][] excelData(String fileName) throws IOException {

		// To open the xlsx sheet from the corresponding location

		XSSFWorkbook book = new XSSFWorkbook("./data/" + fileName + ".xlsx");

		// To go to the sheet name

		XSSFSheet sheet = book.getSheetAt(0);

		// To count the row

		int rowCount = sheet.getLastRowNum();

		System.out.println("Row Count is " + rowCount);

		// To count the column

		int columnCount = sheet.getRow(0).getLastCellNum();

		System.out.println("Column count is " + columnCount);

		// To create object for object class - Step-1

		Object[][] datas = new Object[rowCount][columnCount];

		// To fetch all the data from the excel

		for (int i = 1; i <= rowCount; i++) {

			XSSFRow row = sheet.getRow(i);

			for (int j = 0; j < columnCount; j++) {

				XSSFCell cell = row.getCell(j);

				String stringCellValue = cell.getStringCellValue();

				datas[i - 1][j] = stringCellValue;

				System.out.println(stringCellValue);

			}
		}

		book.close();

		return datas;

	}

}
