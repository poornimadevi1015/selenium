package week3.day1;

import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {

	public static void main(String[] args) {

		// To invoke the browser - Set Path

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		// To launch the browser

		ChromeDriver driver = new ChromeDriver();

		// To maximize the browser

		driver.manage().window().maximize();

		// To invoke the url

		driver.get("http://leaftaps.com/opentaps/control/main");

		// To enter the username

		driver.findElementById("username").sendKeys("DemoSalesManager");

		// To enter the password

		driver.findElementById("password").sendKeys("crmsfa");

		// To click the login button

		driver.findElementByClassName("decorativeSubmit").click();

		// To click on the crm/sfa link

		driver.findElementByLinkText("CRM/SFA").click();

		// To click the Create Lead

		driver.findElementByLinkText("Create Lead").click();

		// To enter the company name

		driver.findElementById("createLeadForm_companyName").sendKeys("TestLeaf");

		// To enter the first name

		driver.findElementById("createLeadForm_firstName").sendKeys("Poornima");

		// To enter the last name

		driver.findElementById("createLeadForm_lastName").sendKeys("Devi");

		// To select the value from the source drop down list - Source

		// First we need to store the element in the WebElement type

		WebElement source = driver.findElementById("createLeadForm_dataSourceId");

		// Using select class we can select the value from the drop down

		Select sc = new Select(source);

		sc.selectByVisibleText("Public Relations");

		// To select the value from the source drop down list - Market Camp

		// First we need to store the element in the WebElement type

		WebElement market = driver.findElementById("createLeadForm_marketingCampaignId");

		// Using select class we can select the value from the drop down

		Select sc1 = new Select(market);

		sc1.selectByVisibleText("Car and Driver");

		// To select the value from the source drop down list - Industry

		// First we need to store the element in the WebElement type

		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");

		// Using select class we can select the value from the drop down

		Select sc2 = new Select(industry);

		// To select a value from the last - collection concept <Size>

		List<WebElement> alloptions = sc2.getOptions();

		int count = alloptions.size();

		sc2.selectByIndex(count - 3);

		// To enter the first name (Local)

		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Poorni");

		// To enter the last name (Local)

		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Devi");

		// To enter the salutation

		driver.findElementById("createLeadForm_personalTitle").sendKeys("Employee");

		// To enter the title

		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("TestEngineer");

		// To enter the department

		driver.findElementById("createLeadForm_departmentName").sendKeys("CORE");

		// To enter the annual revenue

		// driver.findElementById("createLeadForm_annualRevenue").sendKeys("ABCD");

		// To enter the number of employees

		driver.findElementById("createLeadForm_numberEmployees").sendKeys("100");

		// To enter the SIC Code

		driver.findElementById("createLeadForm_sicCode").sendKeys("SIC1001");

		// To click the create lead button

		driver.findElementByName("submitButton").click();

		// To validate the given input in the company name is filled in the form or not
		// or same name or not

		String companyName = driver.findElementById("viewLead_companyName_sp").getText();

		if (companyName.contains("TestLeaf")) {

			System.out.println("Company Name is valid");

		}

		else {

			System.out.println("Company Name is invalid");

		}

		// To close the browser

		driver.close();

	}

}
