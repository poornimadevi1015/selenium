package week3.day1.hw;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcSignUp {

	public static void main(String[] args) throws InterruptedException {

		// To set the path for chrome driver

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		// To launch the chrome browser

		ChromeDriver driver = new ChromeDriver();

		// To maximize the browser

		driver.manage().window().maximize();

		// To invoke the url

		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");

		// To enter the value in the User ID

		driver.findElementById("userRegistrationForm:userName").sendKeys("Poorni15ABCD", Keys.TAB);

		// To enter the value in the password

		driver.findElementById("userRegistrationForm:password").sendKeys("Poorni", Keys.TAB);

		// To enter the value in the confirm password

		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Poorni", Keys.TAB);

		// To select the value in security question from the drop down list

		WebElement securitydrop = driver.findElementById("userRegistrationForm:securityQ");

		Select securityquestion = new Select(securitydrop);

		securityquestion.selectByVisibleText("What is your favorite past-time?");

		// To enter the value in the security answer

		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Watching old movies", Keys.TAB);

		// To select the value in the language drop down list

		WebElement language = driver.findElementById("userRegistrationForm:prelan");

		Select languagedrop = new Select(language);

		languagedrop.selectByValue("en");

		// To enter the value in the first name

		driver.findElementById("userRegistrationForm:firstName").sendKeys("Poornima");

		// To enter the value in the middle name

		driver.findElementById("userRegistrationForm:middleName").sendKeys("D");

		// To enter the value in the last name

		driver.findElementById("userRegistrationForm:lastName").sendKeys("evi");

		// To select the date from the Date Of Birth

		WebElement date = driver.findElementById("userRegistrationForm:dobDay");

		Select datedrop = new Select(date);

		datedrop.selectByValue("15");

		// To select the month from the Date Of Birth

		WebElement month = driver.findElementById("userRegistrationForm:dobMonth");

		Select monthdrop = new Select(month);

		monthdrop.selectByVisibleText("OCT");

		// To select the year from the Date Of Birth

		WebElement year = driver.findElementById("userRegistrationForm:dateOfBirth");

		Select yeardrop = new Select(year);

		yeardrop.selectByVisibleText("1993");

		// To select the occupation from the drop down list - 4th option from the last

		WebElement occupation = driver.findElementById("userRegistrationForm:occupation");

		Select occupationdrop = new Select(occupation);

		List<WebElement> alloptions = occupationdrop.getOptions();

		int count = alloptions.size();

		occupationdrop.selectByIndex(count - 4);

		// To enter the value in the aadhar card number

		driver.findElementById("userRegistrationForm:uidno").sendKeys("1234123412341234");

		// To enter the value in the pan card

		driver.findElementById("userRegistrationForm:idno").sendKeys("1234567890123456");

		// To select the value in the country drop down

		WebElement country = driver.findElementById("userRegistrationForm:countries");

		Select countrydrop = new Select(country);

		countrydrop.selectByValue("94");

		// To load the country extension in phone number

		Thread.sleep(10);

		// To enter the value in the Email

		driver.findElementById("userRegistrationForm:email").sendKeys("abcd1510@gmail.com");

		// To enter the value in the ISD - Mobile

		driver.findElementById("userRegistrationForm:mobile").sendKeys("9876543210");

		// To select the value in the Nationality drop down

		WebElement nationality = driver.findElementById("userRegistrationForm:nationalityId");

		Select nationalitydrop = new Select(nationality);

		nationalitydrop.selectByVisibleText("India");

		// To enter the value in the Flat/Door/No

		driver.findElementById("userRegistrationForm:address").sendKeys("Vilas");

		// To enter the value in the street

		driver.findElementById("userRegistrationForm:street").sendKeys("Krishna Doss Road");

		// To enter the value in the area

		driver.findElementById("userRegistrationForm:area").sendKeys("Perambur");

		// To enter the value in the pin code

		driver.findElementById("userRegistrationForm:pincode").sendKeys("600012", Keys.TAB);

		// To load the state

		Thread.sleep(1000);

		// To select the value in the city drop down

		WebElement city = driver.findElementById("userRegistrationForm:cityName");

		Select citydrop = new Select(city);

		citydrop.selectByVisibleText("Chennai");

		// To load the post office area

		Thread.sleep(1000);

		// To select the value in the post office drop down

		WebElement postoffice = driver.findElementById("userRegistrationForm:postofficeName");

		Select postofficedrop = new Select(postoffice);

		List<WebElement> allopts = postofficedrop.getOptions();

		int optioncount = allopts.size();

		postofficedrop.selectByIndex(optioncount - 3);

		// To enter the value in the phone number

		driver.findElementById("userRegistrationForm:landline").sendKeys("044-26612833");

	}

}
