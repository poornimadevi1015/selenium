package week3.day2;

import java.awt.RenderingHints.Key;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TableInfo {

	public static void main(String[] args) {

		// To set the path

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		// To launch the browser

		ChromeDriver driver = new ChromeDriver();

		// To launch the url

		driver.get("https://erail.in/");

		// To maximize the window

		driver.manage().window().maximize();

		// Implicit Wait

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		// To clear the entered value

		driver.findElementById("txtStationFrom").clear();

		// To enter the From address

		driver.findElementById("txtStationFrom").sendKeys("MAS", Keys.TAB);

		// To clear the entered value

		driver.findElementById("txtStationTo").clear();

		// To enter the To address

		driver.findElementById("txtStationTo").sendKeys("MDU", Keys.TAB);

		// To get the particular table

		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");

		// To get the row size

		List<WebElement> row = table.findElements(By.tagName("tr"));

		// System.out.println("Row Size " + row.size());

		// To get the 2nd column value

		for (WebElement eachRow : row) {

			List<WebElement> column = eachRow.findElements(By.tagName("td"));

			String text2 = column.get(1).getText();

			System.out.println(text2);

		}
	}

}
