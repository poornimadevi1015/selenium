package week3.day2.hw;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindLeads {

	public static void main(String args[]) throws InterruptedException {

		// To set the path

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		// To launch the browser

		ChromeDriver driver = new ChromeDriver();

		// To launch the url

		driver.get("http://leaftaps.com/opentaps/control/main");

		// To maximize the window

		driver.manage().window().maximize();

		// To wait a particular second to load the elements

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

		// To enter the user name

		driver.findElementById("username").sendKeys("DemoSalesManager");

		// To enter the password

		driver.findElementById("password").sendKeys("crmsfa");

		// To click the login button

		driver.findElementByClassName("decorativeSubmit").click();

		// To click the crmsfa link

		driver.findElementByLinkText("CRM/SFA").click();

		// To click the lead link

		driver.findElementByLinkText("Leads").click();

		// To click the find lead link

		driver.findElementByLinkText("Find Leads").click();

		// To click the phone tab

		driver.findElementByXPath("//span[text()='Phone']").click();

		// To enter the country code in the phone number - 9876543210

		driver.findElementByName("phoneCountryCode").clear();

		driver.findElementByName("phoneCountryCode").sendKeys("1");

		// To enter the area code in the phone number

		// driver.findElementByName("phoneAreaCode").sendKeys("044");

		// To enter the phone number

		driver.findElementByName("phoneNumber").sendKeys("9790874566");

		// To click the Find lead button

		driver.findElementByXPath("//button[text()='Find Leads']").click();

		driver.findElementByXPath("//a[text()='10089']").click();

		// To click on the Delete button

		driver.findElementByLinkText("Delete").click();

		driver.findElementByLinkText("Find Leads").click();

		// To click the Name and ID tab

		driver.findElementByXPath("//span[text()='Name and ID']").click();

		// To enter the ID in the ID text box

		driver.findElementByName("id").sendKeys("10696");

		// To click the Find lead button

		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		driver.close();

	}
}
