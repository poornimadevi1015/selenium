package week3.day2.hw;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class EditLead {

	public static void main(String[] args) throws InterruptedException {

		// To set the path

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		// To launch the browser

		ChromeDriver driver = new ChromeDriver();

		// To maximize the window

		driver.manage().window().maximize();

		// To load the page

		driver.manage().timeouts().implicitlyWait(50000, TimeUnit.SECONDS);

		// To invoke the url

		driver.get("http://leaftaps.com/opentaps/control/login");

		// To enter the user name

		driver.findElementById("username").sendKeys("DemoSalesManager");

		// To enter the password

		driver.findElementById("password").sendKeys("crmsfa");

		// To click on the login button

		driver.findElementByClassName("decorativeSubmit").click();

		// To click on the crmsfa link

		driver.findElementByLinkText("CRM/SFA").click();

		// To click on the leads button

		driver.findElementByLinkText("Leads").click();

		// To click on the find leads

		driver.findElementByLinkText("Find Leads").click();

		// To enter the first name

		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Poornima");

		// To click on the find lead button

		driver.findElementByXPath("//button[text()='Find Leads']").click();

		Thread.sleep(5000);

		// To click on the first resulting lead

		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();

		// To verify the title of page

		String title = driver.getTitle();

		System.out.println(title);

		if (title.contains("View Lead | opentaps CRM")) {

			System.out.println("Title is valid");

		}

		else {

			System.out.println("Invalid title");

		}

		// To click on the edit button

		driver.findElementByLinkText("Edit").click();

		// a[text()='Edit']

		// To edit the company name

		WebElement updateCompanyName = driver.findElementById("updateLeadForm_companyName");

		// To clear the company name which is present before

		updateCompanyName.clear();

		// To enter the new company name

		updateCompanyName.sendKeys("Leaf Test");

		// To click on the update button

		driver.findElementByName("submitButton").click();

		String updatedCompanyName = driver.findElementById("viewLead_companyName_sp").getText();

		if (updatedCompanyName.contains("Leaf Test")) {

			System.out.println("Company name is updated");

		}

		else {

			System.out.println("Failed to update company name");

		}

		// To close the browser

		driver.close();

	}

}
