package week3.day2.hw;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {

	public static void main(String args[]) {

		// To set the environment variable

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		// To launch the browser

		ChromeDriver driver = new ChromeDriver();

		// To maximize the window

		driver.manage().window().maximize();

		// To load the page

		driver.manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS);

		// To invoke the url

		driver.get("http://leaftaps.com/opentaps/control/login");

		// To enter the user name

		driver.findElementById("username").sendKeys("DemoSalesManager");

		// To enter the password

		driver.findElementById("password").sendKeys("crmsfa");

		// To click on the login button

		driver.findElementByClassName("decorativeSubmit").click();

		// To click on the crm/sfa link

		driver.findElementByLinkText("CRM/SFA").click();

		// To click the create lead button

		driver.findElementByLinkText("Create Lead").click();

		// To enter the company name

		driver.findElementById("createLeadForm_companyName").sendKeys("Test Leaf");

		// To enter the first name

		driver.findElementById("createLeadForm_firstName").sendKeys("Poornima");

		// To enter the last name

		driver.findElementById("createLeadForm_lastName").sendKeys("Devi");

		// To enter the first name (locale)

		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Devi");

		// To enter the last name (locale)

		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Poornima");

		// To enter the salutation

		driver.findElementById("createLeadForm_personalTitle").sendKeys("Ms");

		// To choose source from drop down list

		WebElement source = driver.findElementById("createLeadForm_dataSourceId");

		Select sourceSelect = new Select(source);

		sourceSelect.selectByVisibleText("Employee");

		// To enter the title

		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Software Test Engineer");

		// To enter the annual revenue

		driver.findElementById("createLeadForm_annualRevenue").sendKeys("100.0");

		// To choose industry from drop down list

		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");

		Select industrySelect = new Select(industry);

		industrySelect.selectByValue("IND_SOFTWARE");

		// To choose ownership

		WebElement ownership = driver.findElementById("createLeadForm_ownershipEnumId");

		Select ownershipSelect = new Select(ownership);

		List<WebElement> allOptions = ownershipSelect.getOptions();

		int count = allOptions.size();

		ownershipSelect.selectByIndex(5);

		// To enter SIC code

		driver.findElementById("createLeadForm_sicCode").sendKeys("VS001");

		// To enter description

		driver.findElementById("createLeadForm_description").sendKeys("Desc");

		// To enter important note

		driver.findElementById("createLeadForm_importantNote").sendKeys("Imp");

		// To enter the country code

		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("91");

		// To enter the area code

		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("1");

		// To enter the extension

		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("044");

		// To enter department

		driver.findElementById("createLeadForm_departmentName").sendKeys("IT");

		// To choose preferred currency from drop down list

		WebElement currency = driver.findElementById("createLeadForm_currencyUomId");

		Select currencySelect = new Select(currency);

		currencySelect.selectByVisibleText("INR - Indian Rupee");

		// To enter number of employees

		driver.findElementById("createLeadForm_numberEmployees").sendKeys("10");

		// To enter the ticker symbol

		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("BFFI");

		// To enter person to ask for

		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("ABI");

		// To enter web url

		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.abi.com");

		// To enter To name

		driver.findElementById("createLeadForm_generalToName").sendKeys("FREEDA ANGELIN");

		// To enter address line1

		driver.findElementById("createLeadForm_generalAddress1").sendKeys("#6 vivekandar street");

		// To enter address line2

		driver.findElementById("createLeadForm_generalAddress2").sendKeys("Dubai kuruku street- dubai main road");

		// To enter city

		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");

		// To choose country

		WebElement country = driver.findElementById("createLeadForm_generalCountryGeoId");

		Select countrySelect = new Select(country);

		countrySelect.selectByValue("IND");

		// To choose state/province

		WebElement state = driver.findElementById("createLeadForm_generalStateProvinceGeoId");

		Select stateSelect = new Select(state);

		stateSelect.selectByValue("IN-TN");

		// To enter the zip postal code

		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600001");

		// To enter the postal code extension

		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("1");

		// To choose market campign

		WebElement market = driver.findElementById("createLeadForm_marketingCampaignId");

		Select marketSelect = new Select(market);

		marketSelect.selectByVisibleText("Catalog Generating Marketing Campaigns");

		// To enter phone number

		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9876543210");

		// To enter email address

		driver.findElementById("createLeadForm_primaryEmail").sendKeys("abcd@gmail.com");

		// To click on the submit button

		driver.findElementByName("submitButton").click();

		// To validate the first name

		String firstNameValidate = driver.findElementByXPath("//span[text()='Poornima']").getText();

		if (firstNameValidate.contains("Poornima")) {

			System.out.println("Entered first name " + firstNameValidate + " is successfully exist in the application");

		}

		else {

			System.out.println("Entered first name " + firstNameValidate + " is failed to exist in the application");
		}

	}

}
