package week3.day2.hw;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class DeleteLead {

	public static void main(String[] args) throws InterruptedException {

		// To Set the path

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		// To launch the browser

		ChromeDriver driver = new ChromeDriver();

		// To maximize the window

		driver.manage().window().maximize();

		// To load the web page

		driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);

		// To invoke the url

		driver.get("http://leaftaps.com/opentaps/control/login");

		// To enter the user name

		driver.findElementById("username").sendKeys("DemoSalesManager");

		// To enter the password

		driver.findElementById("password").sendKeys("crmsfa");

		// To click on the login

		driver.findElementByClassName("decorativeSubmit").click();

		// To click on the crmsfa link

		driver.findElementByLinkText("CRM/SFA").click();

		// To click on the Lead Link

		driver.findElementByLinkText("Leads").click();

		// To click on the Find Lead link

		driver.findElementByLinkText("Find Leads").click();

		// To click on the phone tab

		driver.findElementByXPath("//span[text()='Phone']").click();

		// To enter the country code

		WebElement countryCode = driver.findElementByName("phoneCountryCode");

		countryCode.clear();

		countryCode.sendKeys("1");

		// To enter the area code

		//driver.findElementByXPath("//input[@name='phoneAreaCode']").sendKeys("1");

		// To enter the phone number

		driver.findElementByName("phoneNumber").sendKeys("9876543210");

		// To click on the Find Lead button

		driver.findElementByXPath("//button[text()='Find Leads']").click();

		// To load the page

		Thread.sleep(500);

		// To capture the first resulting lead

		WebElement firstResult = driver
				.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a[1]");

		String firstLead = firstResult.getText();

		System.out.println(firstLead);

		// To click on the first resulting lead

		firstResult.click();

		// To click on the delete button

		driver.findElementByLinkText("Delete").click();

		// To click on the Find Leads button

		driver.findElementByLinkText("Find Leads").click();

		// To enter the captured lead Id

		driver.findElementByName("id").sendKeys(firstLead);

		// To click on the find lead button

		driver.findElementByXPath("//button[text()='Find Leads']").click();

		// To verify the error message

		String errorMessage = driver.findElementByXPath("//div[text()='No records to display']").getText();

		if (errorMessage.contains("No records to display")) {

			System.out.println("The coressponding error message" + errorMessage + "is displayed");

		}

		else {

			System.out.println("Failed to show the error message");

		}
		
		// To close the browser
		
		driver.close();

	}

}
