package week3.day2.hw;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class DuplicateLead {

	public static void main(String[] args) throws InterruptedException {

		// To set the path

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		// To launch the browser

		ChromeDriver driver = new ChromeDriver();

		// To maximize the web page

		driver.manage().window().maximize();

		// To load the page

		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

		// To invoke the url

		driver.get("http://leaftaps.com/opentaps/control/login");

		// To enter the user name

		driver.findElementById("username").sendKeys("DemoSalesManager");

		// To enter the password

		driver.findElementById("password").sendKeys("crmsfa");

		// To click on the login button

		driver.findElementByClassName("decorativeSubmit").click();

		// To click on the crmsfa link

		driver.findElementByLinkText("CRM/SFA").click();

		// To click on the lead link

		driver.findElementByLinkText("Leads").click();

		// To click on the find lead

		driver.findElementByLinkText("Find Leads").click();

		// To click on the email tab

		driver.findElementByXPath("//span[text()='Email']").click();

		// To enter email address

		driver.findElementByName("emailAddress").sendKeys("abcd@gmail.com");

		// To click on the find lead button

		driver.findElementByXPath("//button[text()='Find Leads']").click();

		Thread.sleep(500);

		// To capture the name of the first result

		WebElement fname = driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a[1]");

		String name = fname.getText();

		System.out.println(name);

		// To click on the first resulting lead

		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]").click();

		// To click on the duplicate lead

		driver.findElementByLinkText("Duplicate Lead").click();

		// To get the title and verify

		String title = driver.getTitle();

		if (title.contains("Duplicate Lead")) {

			System.out.println("The corresponding title name is displayed");

		}

		else

		{

			System.out.println("Title name is incorrect");

		}

		// To click on the create lead link

		driver.findElementByName("submitButton").click();

		// To confirm the duplicate name same as captured

		WebElement firstName = driver.findElementByXPath("(//span[@class='tabletext'])[4]");

		String actualFname = firstName.getText();

		System.out.println(actualFname);

		if (name == actualFname) {

			System.out.println("Duplicate lead name is same as captured");

		}

		else {

			System.out.println("Dupliacte name is not same as captured");

		}

		// To close the browser

		driver.close();

	}

}
