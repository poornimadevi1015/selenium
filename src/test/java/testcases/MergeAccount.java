package testcases;

import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class MergeAccount extends ProjectMethods {

	@BeforeClass
	public void tcDetail() {

		testcaseName = "MergeAccount";

		testDesc = "Merging the two accounts";

		author = "Poorni";

		category = "Funtional";

	}

	@Test

	public void account() throws InterruptedException  {
		
		
		click(locateElement("linktext", "CRM/SFA"));

		String str1 = "10591";

		String str2 = "10593";

		// To click on the Accounts tab

		WebElement clickAccount = locateElement("linktext", "Accounts");

		click(clickAccount);

		// To click on the Merge account tab

		WebElement clickMerge = locateElement("linktext", "Merge Accounts");

		click(clickMerge);

		// To click on the from icon

		WebElement fromIcon = locateElement("xpath", "//img[@alt='Lookup']");

		click(fromIcon);

		// To handle the Find Account window

		switchToWindow(1);

		WebElement accountID = locateElement("name", "id");

		type(accountID, str1);

		// To click on the Find Accounts

		WebElement findAccountsFrom = locateElement("xpath", "//button[text()='Find Accounts']");

		click(findAccountsFrom);

		Thread.sleep(3000);

		WebElement firstElement = locateElement("linktext", str1);

		clickwithoutSnap(firstElement);

		// To switch back to the main window

		switchToWindow(0);

		// To click on the to account

		WebElement toIcon = locateElement("xpath", "(//img[@alt='Lookup'])[2]");

		click(toIcon);

		// To handle the Find Account Window

		switchToWindow(1);

		WebElement accountIDTo = locateElement("name", "id");

		type(accountIDTo, str2);

		// To click on the Find Accounts -To

		WebElement findAccountTo = locateElement("xpath", "//button[text()='Find Accounts']");

		clickwithoutSnap(findAccountTo);

		Thread.sleep(3000);

		WebElement firstElementTo = locateElement("linktext", str2);

		clickwithoutSnap(firstElementTo);

		switchToWindow(0);

		// To click on the merge button

		clickwithoutSnap(locateElement("linktext", "Merge"));

		// To click accept in the alert

		acceptAlert();

		// To click on the Find account

		clickwithoutSnap(locateElement("linktext", "Find Accounts"));

		// To enter the from account id

		type(locateElement("name", "id"), str2);

		// To click on the Find button

		click(locateElement("linktext", "Find Accounts"));

		// To verify the display message

		WebElement expected = locateElement("xpath", "//div[@class='x-paging-info']");

		String text = expected.getText();

		if (text.contains("No records to")) {

			System.out.println("Incorrect message");

		}

		else

		{
			System.out.println("The corresponding message is displayed");

		}

	}

}
