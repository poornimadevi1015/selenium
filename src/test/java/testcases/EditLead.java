package testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class EditLead extends ProjectMethods {
	@BeforeClass(groups = "common")
	public void setData() {
		testcaseName = "TC004_EditLead";
		testDesc = "Edit the company name of existing lead ";
		author = "Poornima Devi";
		category = "Smoke";
		excelFileName = "FindLeadData";
	}

	// @Test(dependsOnMethods="testcases.CreateLead.createLead1")

	// @Test(groups="sanity",dependsOnGroups="smoke")

	//Sequential
	
	@Test(dataProvider = "fetchData")

	public void editLead(String fName, String companyName) throws InterruptedException {
		// will be called from @BeforeMethod in ProjectSpecific class which is used to
		// extend the current class login();
		// click crmsfa
		/*
		 * WebElement eleCrmSfa = locateElement("linkText","CRM/SFA"); click(eleCrmSfa);
		 */
		click(locateElement("linktext", "Leads"));
		click(locateElement("linktext", "Find Leads"));
		// locateElement("linkText","Find Leads")
		type(locateElement("xpath", "(//input[@name='firstName'])[3]"), fName);
		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));
		Thread.sleep(5000);
		click(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));
		verifyTitle("View Lead | opentaps CRM");
		click(locateElement("xpath", "//a[contains(text(),'Edit')]"));
		type(locateElement("updateLeadForm_companyName"), companyName);
		click(locateElement("xpath", "//input[@class='smallSubmit']"));

		Thread.sleep(5000);
		verifyPartialText(locateElement("viewLead_companyName_sp"), companyName);

		// will be called from @AfterMethod in ProjectSpecific class which is used to
		// extend the current class closeSession();
	}

	/*@DataProvider(name = "fetchData")
	public String[][] getData() {
		String[][] data = new String[2][2];
		data[0][0] = "Narasimman";
		data[0][1] = "TestLeaf1";
		data[1][0] = "Poornima";
		data[1][1] = "CTS";
		return data;
	}*/
}
