package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class MergeLead extends ProjectMethods {
	
	
	@BeforeClass(groups="common")
	public void setData() {
		testcaseName = "mergeLead";
		testDesc = "merge  the lead ";
		author = "Poornima Devi";
		category = "Smoke";
	}
	
	// To ignore the execution of test case
	
	// Case 1.A - 15-12-2018

   //  @Test(enabled=false)
	
	//@Test(groups="regression",dependsOnGroups="sanity")
	
	// Case 2 - Parallel
	
	@Test

	public void mergeLead() throws InterruptedException {
		
	/*	login();

		WebElement crmLink = locateElement("linktext", "CRM/SFA");

		click(crmLink);
*/
		WebElement leadLink = locateElement("linktext", "Leads");

		click(leadLink);

		WebElement mergeLink = locateElement("linktext", "Merge Leads");

		click(mergeLink);
/*
		WebElement fromIcon = locateElement("xpath", "//img[@alt='Lookup']");

		click(fromIcon);

		switchToWindow(1);
		
		Thread.sleep(5000);

		WebElement fromValue = locateElement("xpath", "(//a[@class='linktext'])[1]");

		click(fromValue);*/
		

		/*
		 * WebElement toIcon = locateElement("xpath", "(//img[@alt='Lookup'])a[1]");
		 * 
		 * click(fromIcon);
		 */

	}

}
