package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class DeleteLead extends ProjectMethods {

	@BeforeClass(groups = "common")
	public void testCaseDetails() {

		testcaseName = "DeleteLead";

		testDesc = "Deleting the lead";

		author = "Poorni";

		category = "Smoke";

	}
//Parallel
	@Test(dataProvider = "fetchData")
	public void delete(String phoneNumber,String code) throws InterruptedException {

		// To click on the Lead Link

		WebElement leadLink = locateElement("linktext", "Leads");

		click(leadLink);

		// To click on the Find Lead link

		WebElement findleadLink = locateElement("linktext", "Find Leads");

		click(findleadLink);

		// To click on the phone tab

		click(locateElement("xpath", "//span[text()='Phone']"));

		// To enter the country code

		WebElement countryCode = locateElement("name", "phoneCountryCode");

		countryCode.clear();

		countryCode.sendKeys(code);

		// To enter the area code

		// driver.findElementByXPath("//input[@name='phoneAreaCode']").sendKeys("1");

		// To enter the phone number

		type(locateElement("name", "phoneNumber"), phoneNumber);

		// To click on the Find Lead button

		click(locateElement("xpath", "//button[text()='Find Leads']"));

		// To load the page

		Thread.sleep(500);

		// To capture the first resulting lead

		WebElement firstResult = locateElement("xpath",
				"(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a[1]");

		String firstLead = firstResult.getText();

		System.out.println(firstLead);

		// To click on the first resulting lead

		click(firstResult);

		// To click on the delete button

		click(locateElement("linktext", "Delete"));

		// To click on the Find Leads button

		click(locateElement("linktext", "Find Leads"));

		// To enter the captured lead Id

		type(locateElement("name", "id"), "firstLead");

		// To click on the find lead button

		click(locateElement("xpath", "//button[text()='Find Leads']"));

		// To verify the error message

		String errorMessage = locateElement("xpath", "//div[text()='No records to display']").getText();

		if (errorMessage.contains("No records to display")) {

			System.out.println("The coressponding error message" + errorMessage + "is displayed");

		}

		else {

			System.out.println("Failed to show the error message");

		}

	}

	@DataProvider(name = "fetchData")
	public String[][] getData() {

		String[][] data = new String[1][2];

		data[0][0] = "9790874566";
		data[0][1] = "1";

		return data;
	}

}
