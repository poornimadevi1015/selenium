package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class CreateLead extends ProjectMethods {

	@BeforeClass(groups = "common")

	public void setData() {
		testcaseName = "createLead";
		testDesc = "creating  the lead ";
		author = "Poornima Devi";
		category = "Smoke";
		excelFileName = "CreateLeadData";
	}

	// To execute the create lead twice

	// @Test(invocationCount = 2, invocationTimeOut=50000)

	// @Test(groups="smoke")

	// @Test(dataProvider="fetchData")

	// Create lead should run 2times within 2minutes - Case 1.b - 15-12-2018

	// @Test(invocationCount=2,timeOut=120000,dataProvider="fetchData")

	// Sequential

	// @Test(dataProvider="fetchData")

	@Test(dataProvider = "fetchData")

	public void createLead1(String company, String fname, String lname) {

		WebElement leadLink = locateElement("linktext", "Create Lead");

		click(leadLink);

		WebElement companyName = locateElement("id", "createLeadForm_companyName");

		type(companyName, company);

		WebElement firstName = locateElement("id", "createLeadForm_firstName");

		type(firstName, fname);

		WebElement lastName = locateElement("id", "createLeadForm_lastName");

		type(lastName, lname);

		WebElement sourceSelect = locateElement("createLeadForm_dataSourceId");

		selectDropDownUsingText(sourceSelect, "Direct Mail");

		WebElement marketSelect = locateElement("createLeadForm_marketingCampaignId");

		selectDropDownUsingIndex(marketSelect, 2);

		WebElement createButton = locateElement("name", "submitButton");

		click(createButton);

	}

	/*
	 * //@DataProvider(name="fetchData")
	 * 
	 * public Object[][] getData() {
	 * 
	 * Object[][] data = new Object[3][3];
	 * 
	 * data[0][0]="TestLeaf"; data[0][1]="Poornima"; data[0][2]="Devi";
	 * 
	 * data[1][0]="Cognizant"; data[1][1]="Abi"; data[1][2]="Rami";
	 * 
	 * data[2][0]="Infosys"; data[2][1]="Suren"; data[2][2]="Deran"; return data;
	 */

//	}

}
