package testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class CreateAccount extends ProjectMethods {

	@BeforeClass
	public void testCaseDetails() {

		testcaseName = "CreateAccount";

		testDesc = "Creating the accounts";

		author = "Poorni";

		category = "Smoke";

	}

	@Test
	public void create() {

		click(locateElement("linktext", "CRM/SFA"));

		// To click on the account

		click(locateElement("linktext", "Accounts"));

		// To click create account

		click(locateElement("linktext", "Create Account"));

		// To enter the value in the account name

		type(locateElement("id", "accountName"), "Poornima Devi");

		// To select the industry

		selectDropDownUsingText(locateElement("id", "industryEnumId"), "Computer Software");

		// To select the currency

		selectDropDownUsingIndex(locateElement("id", "currencyUomId"), 1);

		// To select source

		selectDropDownUsingText(locateElement("id", "dataSourceId"), "Website");

		// To select market

		selectDropDownUsingText(locateElement("id", "marketingCampaignId"), "Demo Marketing Campaign");

		// To enter the phone number

		type(locateElement("id", "primaryPhoneNumber"), "9876543210");

		// To enter city

		type(locateElement("id", "generalCity"), "Chennai");

		// To enter email address

		type(locateElement("id", "primaryEmail"), "abcdNeon@gmail.com");

		// To select the country

		selectDropDownUsingText(locateElement("id", "generalCountryGeoId"), "India");

		// To select the state

		selectDropDownUsingText(locateElement("id", "generalStateProvinceGeoId"), "ANDHRA PRADESH");

		// To click create button

		click(locateElement("classname", "smallSubmit"));

	}

}
