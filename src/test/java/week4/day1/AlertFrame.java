package week4.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertFrame {

	public static void main(String[] args) throws InterruptedException {

		// To set the path

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		// To launch the browser

		ChromeDriver driver = new ChromeDriver();

		// To maximize the window

		driver.manage().window().maximize();

		// To invoke the url

		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");

		// Try it is present in the frame - id, name is also present

		driver.findElementById("iframeResult");

		// Switch to the frame

		driver.switchTo().frame("iframeResult");

		// To click on the Try it button

		driver.findElementByXPath("//button[text()='Try it']").click();

		// To switch to the alert box

		String text = driver.switchTo().alert().getText();

		driver.switchTo().alert().sendKeys("Poornima Devi");

		Thread.sleep(5000);

		System.out.println(text);

		driver.switchTo().alert().accept();

		Thread.sleep(5000);

		// To validate the output

		String output = driver.findElementByXPath("//p[text()='Hello Poornima Devi! How are you today?']").getText();

		if (output.contains("Poornima Devi")) {

			System.out.println("Successful");

		}

		else {

			System.out.println("Failed");
		}

		// To come out of the frame

		driver.switchTo().defaultContent();

		driver.findElementByXPath("//button[text()='Run �']").click();

	}

}
