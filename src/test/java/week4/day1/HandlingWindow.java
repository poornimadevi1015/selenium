package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class HandlingWindow {

	public static void main(String[] args) {

		// To invoke the browser - Set Path

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		// To launch the browser

		ChromeDriver driver = new ChromeDriver();

		// To maximize the browser

		driver.manage().window().maximize();

		// To invoke the url

		driver.get("http://leaftaps.com/opentaps/control/main");

		// To enter the username

		driver.findElementById("username").sendKeys("DemoSalesManager");

		// To enter the password

		driver.findElementById("password").sendKeys("crmsfa");

		// To click the login button

		driver.findElementByClassName("decorativeSubmit").click();

		// To click on the crm/sfa link

		driver.findElementByLinkText("CRM/SFA").click();

		// To click the Create Lead

		driver.findElementByLinkText("Create Lead").click();

		// To enter the company name

		driver.findElementById("createLeadForm_companyName").sendKeys("TestLeaf");

		// To enter the first name

		driver.findElementById("createLeadForm_firstName").sendKeys("Poornima");

		// To enter the last name

		driver.findElementById("createLeadForm_lastName").sendKeys("Devi");

		// To click on the Parent Account

		driver.findElementByXPath("//img[@alt='Lookup']").click();

		// To handle the window

		Set<String> allwindow = driver.getWindowHandles();

		List<String> list = new ArrayList<>();

		list.addAll(allwindow);

		driver.switchTo().window(list.get(1));

		WebElement firstID = driver.findElementByXPath("(//a[@class='linktext'])[1]");

		firstID.click();
		
		driver.switchTo().window(list.get(0));
		
		//String text = firstID.getText();

		// To click the create lead button

		driver.findElementByClassName("smallSubmit").click();
		
		String output = driver.findElementById("viewLead_parentPartyId_sp").getText();

		if (output.contains("10047") ) {

			System.out.println("Successfully created");
		}

		else {

			System.out.println("Failed to create");
		}

	}

}
