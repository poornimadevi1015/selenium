package week4.day1.hw;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) throws InterruptedException {

		// To invoke the browser - Set Path

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		// To launch the browser

		ChromeDriver driver = new ChromeDriver();

		// To maximize the browser

		driver.manage().window().maximize();

		// To invoke the url

		driver.get("http://leaftaps.com/opentaps/control/main");

		// To enter the username

		driver.findElementById("username").sendKeys("DemoSalesManager");

		// To enter the password

		driver.findElementById("password").sendKeys("crmsfa");

		// To click the login button

		driver.findElementByClassName("decorativeSubmit").click();

		// To click on the crm/sfa link

		driver.findElementByLinkText("CRM/SFA").click();

		// To click on the lead link

		driver.findElementByXPath("//a[text()='Leads']").click();

		// To click on the Merge Leads

		driver.findElementByLinkText("Merge Leads").click();

		// To click on the From Lead Icon

		driver.findElementByXPath("//img[@alt='Lookup']").click();

		// To move to the new window

		Set<String> allWindow = driver.getWindowHandles();

		// To get into first window

		List<String> list = new ArrayList<String>();

		list.addAll(allWindow);

		driver.switchTo().window(list.get(1));

		// To enter a value in the id

		driver.findElementByName("id").sendKeys("10732");

		// To click on the Find lead button

		driver.findElementByXPath("//button[text()='Find Leads']").click();

		Thread.sleep(5000);

		driver.findElementByXPath("//a[text()=10732]").click();

		driver.switchTo().window(list.get(0));

		// To click the To Lead icon

		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();

		// To move to the new window

		Set<String> allWindowToIcon = driver.getWindowHandles();

		// To get into first window

		List<String> list1 = new ArrayList<String>();

		list1.addAll(allWindowToIcon);

		driver.switchTo().window(list1.get(1));

		// To enter a value in the id

		driver.findElementByName("id").sendKeys("10734");

		// To click on the Find lead button

		driver.findElementByXPath("//button[text()='Find Leads']").click();

		Thread.sleep(5000);

		driver.findElementByXPath("//a[text()=10734]").click();

		driver.switchTo().window(list.get(0));
		
		// To click on the Merge icon
		
		driver.findElementByClassName("buttonDangerous").click();
		
		// To switch to the alert box

		String text = driver.switchTo().alert().getText();
		
		System.out.println(text);

		driver.switchTo().alert().accept();
		
		

	}

}
