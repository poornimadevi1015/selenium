package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import reports.BasicReport;

public class SeMethods extends BasicReport implements WdMethods {
	public RemoteWebDriver driver;
	public int i = 1;

	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodrivers.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			// System.out.println("The Browser "+browser+" Launched Successfully");
			logStep("The Browser " + browser + " Launched Successfully", "PASS");

		} catch (WebDriverException e) {
			// System.out.println("Browser not Launched Successfully");
			logStep("Browser not Launched Successfully", "FAIL");
			throw new RuntimeException();
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} finally {
			// will take Screenshot even step pass/fail
			takeSnap();
		}

	}

	public WebElement locateElement(String locator, String locValue) {

		try {
			switch (locator) {
			case "id":
				return driver.findElementById(locValue);
			case "classname":
				return driver.findElementByClassName(locValue);
			case "xpath":
				return driver.findElementByXPath(locValue);
			case "linktext":
				return driver.findElementByLinkText(locValue);
			case "cssSelector":
				return driver.findElementByCssSelector(locValue);
			case "name":
				return driver.findElementByName(locValue);
			case "partialLinkText":
				return driver.findElementByPartialLinkText(locValue);
			case "tagName":
				return driver.findElementByTagName(locValue);
			}

		} catch (WebDriverException e) {
			// System.err.println("unKnown Error");
			logStep("Unknown Error", "FAIL");
		} finally {
			takeSnap();

		}
		return null;
	}

	public WebElement locateElement(String locValue) {
		return driver.findElementById(locValue);

	}

	public void type(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			// System.out.println("The Data "+data+" Entered Successfully");
			logStep("The Data " + data + " Entered Successfully", "PASS");
		} catch (Exception e) {
			// System.err.println("Error in Type Action"+e.toString());
			logStep("Error in Type Action" + e.toString(), "FAIL");
		} finally {
			takeSnap();
		}

	}

	public void click(WebElement ele) {
		try {
			ele.click();
			// System.out.println("The Element "+ele+" is clicked Successfully");
			logStep("The Element " + ele.toString() + " is clicked Successfully", "PASS");
		} catch (Exception e) {
			// System.err.println("Error in Click Action"+e.toString());
			logStep("Error in Click Action" + e.toString(), "FAIL");
		} finally {
			takeSnap();
		}
	}

	public void clickwithoutSnap(WebElement ele) {
		try {
			ele.click();
			// System.out.println("The Element "+ele+" is clicked Successfully");
			logStep("The Element " + ele.toString() + " is clicked Successfully", "PASS");
		} catch (Exception e) {
			// System.err.println("Error in Click Action"+e.toString());
			logStep("Error in Click Action" + e.toString(), "FAIL");
		}

	}

	public String getText(WebElement ele) {
		try {
			// System.out.println("The text in the Element "+ele + "is "+ ele.getText());
			logStep("The text in the Element " + ele + "is " + ele.getText(), "PASS");
			return ele.getText();
		} catch (Exception e) {
			// System.err.println("Error in gettext method "+e.toString());
			logStep("Error in gettext method " + e.toString(), "FAIL");
			return e.getMessage();
		} finally {
			takeSnap();

		}

	}

	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select se = new Select(ele);
			se.selectByVisibleText(value);
			// System.out.println(value +" is Selected in the "+ele +"dropdown");
			logStep(value + " is Selected in the " + ele + "dropdown", "PASS");
		} catch (Exception e) {
			// System.err.println("Error in selectDropDownUsingText method "+e.toString());
			logStep("Error in selectDropDownUsingText method " + e.toString(), "FAIL");
		} finally {
			takeSnap();
		}
	}

	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select se = new Select(ele);
			se.selectByIndex(index);
			// System.out.println("Value is Selected using the "+index+" index from the
			// dropdown");
			logStep("Value is Selected using the " + index + " index from the dropdown", "PASS");
		} catch (Exception e) {
			// System.err.println("Error in selectDropDownUsingIndex method "+e.toString());
			logStep("Error in selectDropDownUsingIndex method " + e.toString(), "FAIL");
		} finally {
			takeSnap();
		}

	}

	public boolean verifyTitle(String expectedTitle) {
		try {
			String title = driver.getTitle();
			if (title.equalsIgnoreCase(expectedTitle)) {
				// System.out.println("Title of the Page is " +title + "Matches with "
				// +expectedTitle);
				// takeSnap();
				logStep("Title of the Page is " + title + "Matches with " + expectedTitle, "PASS");
				return true;
			} else {
				// System.out.println("Title of the Page is " +title + "mismatches with "
				// +expectedTitle);
				logStep("Title of the Page is " + title + "mismatches with " + expectedTitle, "FAIL");
				// takeSnap();
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// System.err.println("Error in verifyTitle method "+e.toString());
			logStep("Error in verifyTitle method " + e.toString(), "FAIL");
			return false;
		} finally {
			takeSnap();
		}

	}

	public void verifyExactText(WebElement ele, String expectedText) {
		try {
			String text = ele.getText();
			if (text.equalsIgnoreCase(expectedText)) {
				/*
				 * System.out.println("Text in the Element"+ele +" is Matches with "
				 * +expectedText); takeSnap();
				 */
				logStep("Text in the Element" + ele + " is Matches with " + expectedText, "PASS");

			} else {
				/*
				 * System.out.println("Text in the Element"+ele +" is mismatches with "
				 * +expectedText); takeSnap();
				 */
				logStep("Text in the Element" + ele + " is mismatches with " + expectedText, "FAIL");

			}
		} catch (Exception e) {
			// System.err.println("Error in verifyExactText method "+e.toString());
			logStep("Error in verifyExactText method " + e.toString(), "FAIL");
		} finally {
			takeSnap();
		}

	}

	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
			String text = ele.getText();
			if (text.contains(expectedText)) {
				/*
				 * System.out.println(expectedText +" text Present in the Element"+ele );
				 * takeSnap();
				 */
				logStep(expectedText + " text Present in the Element" + ele, "PASS");
			} else {
				/*
				 * System.out.println(expectedText +"does not exists in the Element"+ele );
				 * takeSnap();
				 */
				logStep(expectedText + "does not exists in the Element" + ele, "FAIL");
			}
		} catch (Exception e) {
			// System.err.println("Error in verifyPartialText method "+e.toString());
			logStep("Error in verifyPartialText method " + e.toString(), "FAIL");
		} finally {
			takeSnap();
		}

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		try {
			ele.isDisplayed();
			logStep("Element displayed in the Page", "PASS");
			return true;
		} catch (Exception e) {
			logStep("Element not displayed in the Page", "FAIL");
			return false;
		} finally {
			takeSnap();
		}
	}

	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		try {
			Set<String> wH = driver.getWindowHandles();
			List<String> lstArray = new ArrayList<>();
			lstArray.addAll(wH);
			driver.switchTo().window(lstArray.get(index));
			// System.out.println("Switched to Window using index" +index);
			logStep("Switched to Window using index" + index, "PASS");

		} catch (Exception e) {
			// System.err.println("Error in switchToWindow method "+e.toString());
			logStep("Error in switchToWindow method " + e.toString(), "FAIL");
		}

	}

	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().frame(ele);
			// System.out.println("Switched to Frame "+ele);
			logStep("Switched to Frame " + ele, "PASS");
		} catch (Exception e) {
			// System.err.println("Error in switchToFrame method "+e.toString());
			logStep("Error in switchToFrame method " + e.toString(), "FAIL");
		} finally {
			takeSnap();
		}

	}

	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
			// System.out.println("Alert Accepted ");
			logStep("Alert Accepted ", "PASS");
		} catch (Exception e) {
			// System.err.println("Error in acceptAlert method "+e.toString());
			logStep("Error in acceptAlert method " + e.toString(), "FAIL");
		} finally {
			takeSnap();
		}

	}

	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
			// System.out.println("Alert Dismissed");
			logStep("Alert Dismissed", "PASS");
		} catch (Exception e) {
			// System.err.println("Error in dismissAlert method "+e.toString());
			logStep("Error in dismissAlert method " + e.toString(), "FAIL");
		} finally {
			takeSnap();
		}

	}

	public String getAlertText() {
		try {
			String txt = driver.switchTo().alert().getText();
			// System.out.println("Text in the Alert is "+ txt);
			logStep("Text in the Alert is " + txt, "PASS");
			return txt;
		} catch (Exception e) {
			// System.err.println("Error in getAlertText method "+e.toString());
			logStep("Error in getAlertText method " + e.toString(), "FAIL");
			return "Error";
		} finally {
			takeSnap();
		}

	}

	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File desc = new File("./snaps/img" + i + ".png");
			FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	public void closeBrowser() {
		try {
			driver.close();
			// System.out.println("Closes the current Active Browser Window");
			logStep("Closes the current Active Browser Window", "PASS");
		} catch (Exception e) {

			// System.err.println("Error in closeBrowser method "+e.toString());
			logStep("Error in closeBrowser method " + e.toString(), "FAIL");
		}

	}

	public void closeAllBrowsers() {
		try {
			driver.quit();
			// System.out.println("Closes all the Browser window opened in this Current
			// Session");
			logStep("Closes all the Browser window opened in this Current Session", "PASS");
		} catch (Exception e) {
			// System.err.println("Error in closeAllBrowsers method "+e.toString());
			logStep("Error in closeAllBrowsers method " + e.toString(), "FAIL");
		}
	}

	public WebElement selectTable(WebElement ele) {
		try {
			System.out.println("Table found " + ele);
			return ele;
		} catch (Exception e) {
			System.err.println("Error in selectTable method " + e.toString());
		} finally {
			takeSnap();
		}
		return null;
	}

	public WebElement selectRow(WebElement ele, int rowIndex, int colIndex) {
		try {
			List<WebElement> rows = ele.findElements(By.tagName("tr"));
			WebElement eleRow = rows.get(rowIndex);
			System.out.println("Row Selected from the Table using index " + rowIndex);
			return eleRow;
		} catch (Exception e) {
			System.err.println("Error in selectRow method " + e.toString());
		}
		return null;

	}

}
