package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.beust.jcommander.Parameter;

import excel.LearnExcel;

public class ProjectMethods extends SeMethods {

	@DataProvider(name = "fetchData")

	public static Object[][] excel() throws IOException {

		return LearnExcel.excelData(excelFileName);

	}

	@Parameters({ "browser", "url", "uname", "pwd" })
	@BeforeMethod(groups = "common")
	public void login(String browserName, String URL, String userName, String passWord) throws InterruptedException {
		startApp(browserName, URL);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, userName);
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, passWord);
		WebElement elelogin = locateElement("classname", "decorativeSubmit");
		click(elelogin);
		click(locateElement("linktext", "CRM/SFA"));
	}

	@AfterMethod(groups = "common")
	public void close() {
		closeBrowser();
	}

}
